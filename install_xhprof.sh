#!/bin/bash

workdir=$(pwd);

# install only with sudo
if [ $(whoami) != 'root' ]; then
  echo "You must be root to install xhprof"
  exit -1
fi

# get the packages
apt-get install php5-dev graphviz

# fallback solution: phpize does work shitty when installing with pecl :(

dir="xhprof-0.9.2"
file="$dir.tgz"

wget -O $workdir/$file http://pecl.php.net/get/$file
if [ ! -e "$workdir/$file" ]; then
  echo "Could not write \"$file\""
  exit -1
fi

tar xf $file
cd ./$dir/extension/
phpize
./configure --with-php-config=/usr/bin/php-config5
make > /dev/null
make install > /dev/null

cd $workdir
pwd
if [[ -e "$dir/xhprof_html" && -e "$dir/xhprof_lib" ]]; then
  mv $dir/xhprof_html /usr/share/php/
  mv $dir/xhprof_lib /usr/share/php/
  wget -O /usr/share/php/xhprof_html/php.jpg http://blog.fieldid.com/wp-content/uploads/safety-inspections.jpg
  rm -rf $dir
  rm -rf $file
else
  echo "UI or Lib files not available"
  rm -rf $dir
  rm -rf $file
  exit -1
fi

echo "add xhprof to php.ini "
cat > /etc/php5/apache2/conf.d/xhprof.ini <<INPUT
 
extension=xhprof.so
xhprof.output_dir="/var/www/xhprof/tmp/xhprof"
INPUT

echo "create xhprof web dir"
mkdir /var/www/xhprof
mkdir /var/www/xhprof/include
mkdir /var/www/xhprof/www
ln -s /usr/share/php/xhprof_html /var/www/xhprof/www/xhprof

echo "creating xhprof.php"
cat > /var/www/xhprof/include/xhprof.php <<INPUT
<?php
/**
 * @author mratz <m.ratz@bigpoint.net>
 * @author araap <a.raap@bigpoint.net>
 */

if (extension_loaded('xhprof')) {
    include_once '/usr/share/php/xhprof_lib/utils/xhprof_lib.php';
    include_once '/usr/share/php/xhprof_lib/utils/xhprof_runs.php';
    xhprof_enable(XHPROF_FLAGS_CPU + XHPROF_FLAGS_MEMORY);

    function xhprofHandler() {
        // take something else here if you want any other auto-namespacing
        \$parts = explode('.', \$_SERVER['HTTP_HOST']);
        \$profiler_namespace = \$parts[0];
        
        \$xhprof_data = xhprof_disable();
        \$xhprof_runs = new XHProfRuns_Default();
        \$xhprof_runs->save_run(\$xhprof_data, \$profiler_namespace);
    }

    register_shutdown_function('xhprofHandler');
}
INPUT

echo "creating index.php for XHProf UI"
cat > /var/www/xhprof/www/index.php <<INPUT
<?php

/**
 * @author mratz <m.ratz@bigpoint.net>
 * @author araap <a.raap@bigpoint.net>
 */
 
\$runner = new DirectoryIterator(dirname(__FILE__) . '/../tmp/xhprof');
\$templateLink = '<a href="http://' . \$_SERVER['HTTP_HOST'] . '/xhprof/?run=%s&source=%s">%s</a>';
\$templateRow = '<tr><td>%s</td><td>%s</td><td>%s</td><td><input type="checkbox" name="%s" /></td></tr>';
\$files = array();

function human_filesize(\$bytes, \$decimals = 2) {
  \$sz = ' KMGTP';
  \$factor = floor((strlen(\$bytes) - 1) / 3);
  return sprintf("%.{\$decimals}f", \$bytes / pow(1024, \$factor)) . @\$sz[\$factor];
}

foreach (\$runner as \$fileInfo) {
	if (\$fileInfo->isFile()) {
		\$splits = explode('.', \$fileInfo->getFileName());
		
		// assemble link 
		\$splits[0] = sprintf(\$templateLink, \$splits[0], \$splits[1], \$splits[0]);
		
		// asseble line
		\$splits[0] = sprintf(
			\$templateRow,
			\$splits[0],
			human_filesize(\$fileInfo->getSize(), 0),
			date('d.M.Y H:i:s', \$fileInfo->getMTime()),
			\$splits[1]
		);
		\$files[\$splits[1]][\$splits[0]] = \$fileInfo->getMTime();
	}	
}

\$output = '<html>';
\$output .= '<h1>XHProf Overview</h1>' . PHP_EOL;

foreach (\$files as \$namespace => &\$fileList) {
	\$output .= '<table><thead><th colspan="2">' . \$namespace . '</th></thead>';
	arsort(\$fileList); // haha, pirate sort :D
	\$rowList = array_keys(\$fileList);
	foreach (\$rowList as \$row) {
		\$output .= \$row . PHP_EOL;
	}
	\$output .= '</table>' . PHP_EOL;
}
\$output .= '</html>';

echo \$output;
INPUT

echo "update system entries"
/etc/init.d/apache2 restart

echo "add tmp dir"
if [ ! -e "/var/www/xhprof/tmp/xhprof" ]; then
  mkdir -p /var/www/xhprof/tmp/xhprof
  chmod -R 777 /var/www/xhprof/tmp/xhprof
fi

cat <<INPUT



usage: 
* open your projects vhost.conf
* add the following line directly above the closing <virtualhost>-tag:
    php_admin_value auto_prepend_file /var/www/xhprof/include/xhprof.php
* look here (http://tinyurl.com/cv9klaa) for more info in using xhprof

install script for XHProf created by Agata Raap & Maximilian Ratz
INPUT

